document.querySelector('.js-add-item').addEventListener(
  'click',
  event => {
    const mode = event.target.getAttribute('data-mode');
    const inputText = document.querySelector('.js-input').value;

    if (mode === 'add') {
      const input = document.createElement('input');
      input.type = 'checkbox';
      input.classList.add('form-check-input');
    
      const spanText = document.createElement('span');

      console.log('Hello');

      spanText.classList.add('text')
      spanText.textContent = inputText;
    
      const spanRemove = document.createElement('span');
      spanRemove.classList.add('remove');
      spanRemove.textContent = 'x';

      const id = document.querySelector('.js-list').children.length + 1; 
    
      const li = document.createElement('li');
      li.setAttribute('id', 'item-' + id)
      li.append(input);
      li.append(spanText);
      li.append(spanRemove);
  
      document.querySelector('.js-list').append(li);
  
      document.querySelector('.js-input').value = '';

      return;
    } 

    if (mode === 'refresh') {
      const id = event.target.getAttribute('data-item-id');

      document.querySelector('#' + id + ' .text').textContent = inputText;
      document.querySelector('.js-input').value = '';

      event.target.textContent = 'Добавить';
      event.target.setAttribute('data-mode', 'add');
      event.target.removeAttribute('data-item-id');

      event.target.classList.remove('btn-warning');
      event.target.classList.add('btn-primary');

      return;
    }
  }
);


document.querySelector('.js-list').addEventListener(
  'click',
  event => {
    if (!event.target.classList.contains('remove')) {
      return;
    }

    event.target.closest('li').remove();
  }
);

document.querySelector('.js-list').addEventListener(
  'click',
  event => {
    if (!event.target.classList.contains('form-check-input')) {
      return;
    }
    
    event.target
      .closest('li')
      .querySelector('.text')
      .classList.toggle('done');
  }
);

document.querySelector('.js-list').addEventListener(
  'click',
  event => {
    if (!event.target.classList.contains('text')) {
      return;
    }

    const text = event.target.textContent;
    const id = event.target.closest('li').getAttribute('id');

    document.querySelector('.js-input').value = text;

    document.querySelector('.js-add-item').textContent = 'Обновить';
    document.querySelector('.js-add-item').classList.remove('btn-primary');
    document.querySelector('.js-add-item').classList.add('btn-warning');
    document.querySelector('.js-add-item').setAttribute('data-mode', 'refresh')
    document.querySelector('.js-add-item').setAttribute('data-item-id', id)
  }
);

document.querySelector('.js-cancel-update-mode').addEventListener(
  'click',
  () => {
    const input = document.querySelector('.js-input');
    const button = document.querySelector('.js-add-item');

    button.removeAttribute('data-item-id');
    button.setAttribute('data-mode', 'add');
    button.classList.remove('btn-warning');
    button.classList.add('btn-primary');

    input.value = '';
  }
)

